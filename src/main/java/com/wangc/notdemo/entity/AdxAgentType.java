package com.wangc.notdemo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wangchuang
 * @date 2022/4/11 15:31
 * @description: TODO
 */
@Data
@Builder
@AllArgsConstructor
public class AdxAgentType implements Serializable {
    public AdxAgentType() {

    }
    private static final long serialVersionUID = 1L;
    /**
     * 经纪人标签
     */
    String tags;

    /**
     * 400是否达标
     */
    Integer phoneQualified;
    List<String> resblockIds;
    Map<Integer, String> maps;
    Company company;

    /**
     * 满意度得分
     */
    Double feedbackScore;

    /**
     * vip等级 0非会员 1轻享会员 2尊享会员 3豪享会员
     */
    Integer vipLevel;
    private String ucId;
    private String userCode;
    private String reason;
    private String agentMark;
    private String houseRole;
    private String digV;
    private String flowType;
    private Boolean proofComplete;
    private String port;
    private String name;
    /**
     * 经纪人领域
     *
     */
    private String avatar;
    private String mobile;
    private String brand;
    private String jobTitle;
    private String positionCode;
    private String positionName;
    private String officeAddress;
    private String officeAddressName;

    private String compName;
    private String compFullName;
    private String resblockId;
    private Long bizcircleId;
    private Long stationId;
    private String houseCode;
    private Long houseId;
    private String style;
    private String agentToken;
    private String shopCode;
    private String shopName;
    private String rentApartmentAgentType;
    /**
     * 经纪人店铺地址
     */
    private String dianpuUrl;
    /**
     * 经济人主要证件信息
     */
    private Map<String, Object> primaryProof;
    private List<HashMap<String, Object>> proofListWithSort;
    private List<HashMap<String, Object>> agentProofList;
    private List<HashMap<String, Object>> orgProofList;
    /**
     * 特长标签
     */
    private List<String> specialtyList;
    /**
     * 话务号码
     */
    private String phone400;
    private int usefulCount;
    /**
     * 贝壳分
     */
    private Integer keQuality;
    /**
     * 品牌编码
     */
    private String brandCode;


    private HashMap<String, Object> extAgentProofMap;

    private HashMap<String, String> extAgentProofTitleMap;

}

