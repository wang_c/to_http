package com.wangc.notdemo.entity.params;

import com.wangc.notdemo.entity.Company;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author wangchuang
 * @date 2022/4/11 15:42
 * @description: AgentQueryParam
 */
@Data
public class AgentQueryParam {
    /**
     * 门店编码
     */
    String shopCode;

    private Integer adId;

    private Map<Integer, String> maps;
    private Company company;

    private String hdicCityId;

    private List<String> resblockIds;

    private List<String> houseCodes;

    /**
     * 人相关
     */
    private String ucId;

    private List<Long> appointUcId;

    /**
     * vr带看预约单预约开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date orderDate;

    /**
     * 营销大区编码，非大区Id
     */
    private String marketingCode;
}
