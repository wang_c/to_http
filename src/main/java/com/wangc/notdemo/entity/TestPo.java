package com.wangc.notdemo.entity;

import lombok.Data;

@Data
public class TestPo {
    private String id;

    private String name;

    private String lists;
}
