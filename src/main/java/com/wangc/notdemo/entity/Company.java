package com.wangc.notdemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author wangchuang
 * @date 2022/1/25 12:05
 * @description: 实体类公司
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Company {
    private String name;

    private String address;

    private List<String> lists;

    private Map<String, String> companyMap;
}
