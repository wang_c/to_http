package com.wangc.notdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotDemoApplication.class, args);
    }

}
