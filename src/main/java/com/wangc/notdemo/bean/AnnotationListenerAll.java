package com.wangc.notdemo.bean;

import com.wangc.notdemo.service.annotation.ToHttp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import java.util.Map;
import java.util.Objects;

/**
 * @author wangchuang
 * @date 2022/3/31 18:18
 * @description: AnnotationListener姿势
 */
@Slf4j
@Component
public class AnnotationListenerAll implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    RequestMappingHandlerMapping requestMappingHandlerMapping;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        /**
        log.info("============={}", event.getApplicationContext());
        Map<String, Object> beansWithAnnotation = event.getApplicationContext()
                .getBeansWithAnnotation(ToHttp.class);

        //方法一：可以获取非代理类的注解，但无法获取代理类的注解
        for (Object value : beansWithAnnotation.values()) {
            ToHttp toHttp = AnnotationUtils.getAnnotation(value.getClass(), ToHttp.class);
            if (Objects.isNull(toHttp)) {
                log.error("toHttp is Null");
            } else {
                log.info("toHttp is not Nul");
            }
        }

        //方法二：可以获取代理类和非代理类的注解
        for (String beanName : beansWithAnnotation.keySet()) {
            ConfigurableListableBeanFactory clbf = event.getApplicationContext().getBeanFactory();
            Object value = clbf.getSingleton(beanName);
            if (value != null) {
                ToHttp toHttp = AnnotationUtils.findAnnotation(value.getClass(), ToHttp.class);
                if (toHttp == null) {
                    log.error("beanFactroy: toHttp is Null");
                } else {
                    log.info("beanFactroy: toHttp is not Nul");
                }
            }
        }

        //方法三：可以获取代理类的注解，但无法获取非代理类的注解
        try {
            for (Object value : beansWithAnnotation.values()) {
                //仅适用于代理类
                ToHttp toHttp = Class.forName(value.getClass().getGenericSuperclass().getTypeName()).getAnnotation(ToHttp.class);
                if (toHttp == null) {
                    log.error("Class.forName: toHttp is Null");
                } else {
                    log.info("Class.forName: toHttp is not Nul");
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } */
    }
}
