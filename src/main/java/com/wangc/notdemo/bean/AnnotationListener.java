package com.wangc.notdemo.bean;

import com.wangc.notdemo.service.annotation.ToHttp;
import com.wangc.notdemo.service.annotation.ToHttpClass;
import com.wangc.notdemo.utils.JavassistUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author wangchuang
 * @date 2022/3/31 18:18
 * @description: AnnotationListener
 */
@Slf4j
@Component
public class AnnotationListener implements ApplicationListener<ApplicationReadyEvent> {
    private static final Set<String> HTTP_PATH_SET = new HashSet<>();
    @Autowired
    RequestMappingHandlerMapping requestMappingHandlerMapping;

    @SneakyThrows
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        log.info("============================");
        log.info("ApplicationReadyEvent: {}", event.getApplicationContext());
        // 先获取注解的类
        Map<String, Object> beansWithAnnotation = event.getApplicationContext()
                .getBeansWithAnnotation(ToHttpClass.class);

        for (Object value : beansWithAnnotation.values()) {
            // 方法参数添加 requestBody注解
            JavassistUtil.addAnnotationToMethodParams(value.getClass(), getToHttpMethods(value.getClass()), RequestBody.class);
            // 需要再获取一遍
            Method[] toHttpMethods = getToHttpMethods(value.getClass());
            for (Method method : toHttpMethods) {
                ToHttp toHttp = AnnotationUtils.getAnnotation(method, ToHttp.class);
                if (Objects.isNull(toHttp)) {
                    continue;
                }
                if (HTTP_PATH_SET.contains(toHttp.path())) {
                    log.error("toHttp annotation duplicate path! path: {}, method: {}", toHttp.path(), method);
                    throw new RuntimeException("toHttp annotation duplicate path !");
                }
                HTTP_PATH_SET.add(toHttp.path());
                try {
                    RequestMappingInfo requestMappingInfo = RequestMappingInfo
                            .paths(toHttp.path())
                            .methods(RequestMethod.GET)
                            .build();
                    // 注册mapping对象
                    requestMappingHandlerMapping.registerMapping(
                            requestMappingInfo, value.getClass().newInstance(), method);
                } catch (Exception e) {
                    log.error("register RequestMapping fail ! error: {}", e.getMessage());
                }
            }
        }
    }

    private Method[] getToHttpMethods(Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredMethods())
                .filter(method -> method.isAnnotationPresent(ToHttp.class))
                .toArray(Method[]::new);
    }
}
