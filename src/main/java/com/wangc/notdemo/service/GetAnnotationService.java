package com.wangc.notdemo.service;

import com.wangc.notdemo.service.annotation.PreExecute;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.lang.annotation.Annotation;

/**
 * @author wangchuang
 * @date 2021/12/21 15:45
 * @description: 获取注解
 */
@Service
@Slf4j
public class GetAnnotationService {
    public static void main (String[] args) {
        TestService testService = new TestService();
        TestServiceSon testServiceSon = new TestServiceSon();
        // getAnnotations
        Annotation[] annotations = testService.getClass().getAnnotations();
        for (Annotation annotation : annotations) {
            log.info("注解：" + annotation);
        }
        Annotation[] sonAnnotations = testServiceSon.getClass().getAnnotations();
        for (Annotation sonAnnotation : sonAnnotations) {
            log.info("子类注解：" + sonAnnotation);
        }



        // getAnnotation
        Annotation annotation = testService.getClass().getAnnotation(PreExecute.class);
        log.info("注解PreExecute：" + annotation);

        // getAnnotations
        Annotation[] declaredAnnotations = testService.getClass().getDeclaredAnnotations();
        for (Annotation declaredAnnotation : declaredAnnotations) {
            log.info("声明式注解：" + declaredAnnotation);
        }

        Annotation[] sonDeclaredAnnotations = testServiceSon.getClass().getDeclaredAnnotations();
        for (Annotation sonDeclaredAnnotation : sonDeclaredAnnotations) {
            log.info("子类声明式注解：" + sonDeclaredAnnotation);
        }
    }
}
