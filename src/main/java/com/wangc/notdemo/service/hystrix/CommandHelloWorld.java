package com.wangc.notdemo.service.hystrix;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;

/**
 * @author wangchuang
 * @date 2021/12/17 11:45
 * @description: hystrix hello world
 */
public class CommandHelloWorld extends HystrixCommand<String> {

    private final String name;

    public CommandHelloWorld(String name) {
        super(HystrixCommandGroupKey.Factory.asKey("ExampleGroup"));
        this.name = name;
    }

    @Override
    protected String run() {
        // a real example would do work like a network call here
        return "Hello " + name + "!";
    }

    public static void main(String[] args) {
        String[] test1 = new String[]{"a","b", "c"};
        String[] test2 = test1;
        test2[1] = "d";
        System.out.println("test2:");
        for (String item : test2) {
            System.out.println(item);
        }
        for (String item: test1) {
            System.out.println(item);
        }
    }
}