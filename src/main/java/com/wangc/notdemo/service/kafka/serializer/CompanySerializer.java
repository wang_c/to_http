package com.wangc.notdemo.service.kafka.serializer;

import com.wangc.notdemo.entity.Company;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serializer;
import org.springframework.stereotype.Service;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * @author wangchuang
 * @date 2022/1/25 12:06
 * @description: 自定义序列化器Company
 */
@Service
@Slf4j
public class CompanySerializer implements Serializer<Company> {
    @Override
    public void configure(Map config, boolean isKey) {
        // nothing
    }

    @Override
    public byte[] serialize(String topic, Company data) {
        if (data == null) {
            return null;
        }
        byte[] name;
        byte[] address;
        try {
            if (data.getName() != null) {
                name = data.getName().getBytes(StandardCharsets.UTF_8);
            } else {
                name = new byte[0];
            }

            if (data.getAddress() != null) {
                address = data.getAddress().getBytes(StandardCharsets.UTF_8);
            } else {
                address = new byte[0];
            }
            ByteBuffer buffer = ByteBuffer.allocate(4 + 4 + name.length + address.length);
            buffer.putInt(name.length);
            buffer.put(name);
            buffer.putInt(address.length);
            buffer.put(address);
            return buffer.array();
        } catch (Exception e) {
            log.error("kafka CompanySerializer serializer error : {}", e.getMessage());
        }
        // ProducerBatch producerBatch = new ProducerBatch();
        // RecordAccumulator recordAccumulator = new RecordAccumulator();
        // ProduceRequest produceRequest = new ProduceRequest();
        return new byte[0];
    }

    @Override
    public void close() {
        // nothing
    }
}
