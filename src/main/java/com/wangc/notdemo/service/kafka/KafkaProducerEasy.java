package com.wangc.notdemo.service.kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.stereotype.Service;

import java.util.Properties;

/**
 * @author wangchuang
 * @date 2022/1/21 17:12
 * @description: TODO
 */
@Slf4j
@Service
public class KafkaProducerEasy {
    private static final String BROKER_LIST = "localhost:9092";

    private static final String TOPIC = "test";

    private Properties initProperties() {
        Properties properties = new Properties();
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BROKER_LIST);
        return properties;
    }
    public void sendMsg(Object msg) {
        Properties properties = initProperties();

        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(properties);
        ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC, msg.toString());

        try {
            kafkaProducer.send(record);
        } catch (Exception e) {
            log.error("kafka send msg error, topic : {}, error: {}", TOPIC, e.getMessage());
        }
    }
}
