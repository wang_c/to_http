package com.wangc.notdemo.service.aviator;

import com.google.common.collect.Maps;
import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.AviatorEvaluatorInstance;
import com.googlecode.aviator.Expression;
import com.wangc.notdemo.entity.TestPo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.Optional;

/**
 * @author wangchuang
 * @date 2021/11/10 11:42
 * @description: aviator表达式
 */
@Slf4j
public class AviatorService {
    private static AviatorEvaluatorInstance AVIATOR_INSTANCE;

    public static boolean execute(String aviatorExpress) {
        if (StringUtils.isEmpty(aviatorExpress)) {
            return true;
        }
        try {
            Expression expression = AviatorEvaluator.compile(aviatorExpress);
            return Optional.ofNullable(expression.execute()).map(o -> (Boolean) o).orElse(false);
        } catch (Exception e) {
            log.error("aviator express error,errorMsg:{},aviator express:{}", e.getMessage(), aviatorExpress);
            return false;
        }
    }

    public static Object execute2(String aviatorExpress) {
        if (StringUtils.isEmpty(aviatorExpress)) {
            return true;
        }
        try {
            Expression expression = AviatorEvaluator.compile(aviatorExpress);
            return Optional.ofNullable(expression.execute()).orElse(null);
        } catch (Exception e) {
            log.error("aviator express error,errorMsg:{},aviator express:{}", e.getMessage(), aviatorExpress);
            return false;
        }
    }

    public static Object executeMap(String aviatorExpress, Map<String, Object> map) {
        if (MapUtils.isEmpty(map)) {
            return true;
        }
        try {
            Expression expression = AviatorEvaluator.compile(aviatorExpress);
            return Optional.ofNullable(expression.execute(map)).map(String::valueOf).orElse("");
        } catch (Exception e) {
            log.error("aviator express error,errorMsg:{},aviator express:{}", e.getMessage(), aviatorExpress);
            return false;
        }
    }

    public static void main(String[] args) {
        System.out.println(execute("string.length('helloooo') < 10"));
        Map<String, Object> map = Maps.newHashMap();
        TestPo testPo = new TestPo();
        testPo.setLists("202011050010");
        map.put("request", testPo);
        System.out.println(executeMap("include(seq.list('202011050003','202011050012','202011050010','202011050006'),request.marketingCode)?'1':'0'", map));
    }
}
