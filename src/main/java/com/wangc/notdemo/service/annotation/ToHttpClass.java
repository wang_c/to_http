package com.wangc.notdemo.service.annotation;

import java.lang.annotation.*;

/**
 * @author wangchuang
 * @date 2022/4/1 15:07
 * @description: ToHttpClass
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ToHttpClass {
}
