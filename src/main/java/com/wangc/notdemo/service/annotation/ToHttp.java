package com.wangc.notdemo.service.annotation;

import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.annotation.*;

/**
 * @author wangchuang
 * @date 2022/3/24 20:26
 * @description: ToHttp
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@ResponseBody
public @interface ToHttp {
    String path();
}
