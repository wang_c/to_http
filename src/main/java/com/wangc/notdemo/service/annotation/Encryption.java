package com.wangc.notdemo.service.annotation;

import java.lang.annotation.*;

/**
 * @author wangchuang
 * @date 2021/12/21 16:19
 * @description: 加密注解
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Encryption {
    /**
     * 加密类型
     */
    String value() default "AES";
}
