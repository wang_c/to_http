package com.wangc.notdemo.service.annotation;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @author wangchuang
 * @date 2021/12/21 16:31
 * @description: TODO
 */
@Component
@Slf4j
@Aspect
public class EncryptionAspect {
    @Pointcut("@annotation(com.wangc.notdemo.service.annotation.Encryption)")
    public void encryptionPointcut() {

    }

    @Before("encryptionPointcut()")
    public void before() {
        log.info("before: 方法执行之前！");
    }

    @After("encryptionPointcut()")
    public void after() {
        log.info("after: 方法执行完之后");
    }

    @AfterReturning("encryptionPointcut()")
    public void afterReturn() {
        log.info("after return : 方法返回之后");
    }

//    @Around("encryptionPointcut()")
//    public int dealProcess(ProceedingJoinPoint joinPoint) {
//        log.info("around 开始执行");
//        Object result = null;
//        // 获取注解
//        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
//        Method method = methodSignature.getMethod();
//        Encryption annotation = method.getAnnotation(Encryption.class);
//
//        // 如果被标识了，则进行加密
//        if(annotation != null) {
//            // 进行加密
//            String encrypt = null;
//            switch (annotation.value()) {
//                case "AES":
//                    log.info("AES");
//                    break;
//                case "DES":
//                    log.info("DES");
//                    break;
//                default:
//                    log.info("DEFAULT");
//                    break;
//            }
//        }
//        return 0;
//    }
}
