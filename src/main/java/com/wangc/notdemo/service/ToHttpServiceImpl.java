package com.wangc.notdemo.service;

import com.wangc.notdemo.entity.AdxAgentType;
import com.wangc.notdemo.entity.Company;
import com.wangc.notdemo.entity.params.AgentQueryParam;
import com.wangc.notdemo.service.annotation.ToHttp;
import com.wangc.notdemo.service.annotation.ToHttpClass;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author wangchuang
 * @date 2022/3/24 19:53
 * @description: ToHttpServiceImpl
 */
@Service
@ToHttpClass
public class ToHttpServiceImpl implements ToHttpService {

    @Override
    @ToHttp(path = "test02")
    public String toHttp2(String param2, AgentQueryParam param1) {
        return param2;
    }

    @Override
    @ToHttp(path = "/getAgentByParams")
    public AdxAgentType toHttp1(AgentQueryParam agentQueryParam, Integer param1, int param2, Double param3) {
        AdxAgentType adxAgentType = new AdxAgentType();
        adxAgentType.setUcId(agentQueryParam.getUcId());
        adxAgentType.setShopCode(agentQueryParam.getShopCode());
        adxAgentType.setResblockIds(agentQueryParam.getResblockIds());
        adxAgentType.setMaps(agentQueryParam.getMaps());
        adxAgentType.setCompany(agentQueryParam.getCompany());
        adxAgentType.setSpecialtyList(new ArrayList<>(Arrays.asList("1","2","3")));
        return adxAgentType;
    }
}
