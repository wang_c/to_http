package com.wangc.notdemo.service.lazyinitial;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author wangchuang
 * @date 2021/9/6 11:01
 * @description: 一个service
 */
@Service
public class ServiceA {
    @Resource
                    @Lazy
    ServiceB serviceB;
}
