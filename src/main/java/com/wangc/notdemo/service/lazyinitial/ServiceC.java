package com.wangc.notdemo.service.lazyinitial;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author wangchuang
 * @date 2021/9/6 11:02
 * @description: TODO
 */
@Service
public class ServiceC {
    @Resource
    @Lazy
    ServiceA serviceA;
}
