package com.wangc.notdemo.service;

import com.google.common.collect.Maps;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author wangchuang
 * @date 2021/11/10 18:58
 * @description: 子类
 */
@Service
public class TestServiceSon extends TestService {
    private static final Map<String, String> EXPRESSION_MAP = Maps.newHashMap();

    public static void main(String[] args) {
    }
}
