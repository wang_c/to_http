package com.wangc.notdemo.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.wangc.notdemo.entity.Company;
import com.wangc.notdemo.entity.TestPo;
import com.wangc.notdemo.proxy.SendMsgService;
import com.wangc.notdemo.proxy.SendMsgServiceImpl;
import com.wangc.notdemo.proxy.cglib.CglibProxyFactory;
import com.wangc.notdemo.service.annotation.Encryption;
import com.wangc.notdemo.service.annotation.PreExecute;
import com.wangc.notdemo.service.annotation.ToHttp;
import com.wangc.notdemo.service.spi.Superman;
import com.wangc.notdemo.utils.JacksonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * @author wangchuang
 * @description: 测试
 * @Date 2021/9/6 10:58
 */
@Service
@Slf4j
@PreExecute
public class TestService {
    private static final String REAL_TIME_ADX_ASSIGN_SJ_RESULT_INDEX = "commerce-realtime-oppty_%s";

    private static final Map<String, String> EXPRESSION_MAP = Maps.newHashMap();

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    SendMsgService sendMsgServiceImpl;

    @Encryption
    public static int testEncryption() {
        log.info("被注解的方法开始执行！");
        return 0;
    }

    @Encryption
    public  int testEncryption1() {
        log.info("方法本身： 开始执行！");
        return 0;
    }

    public String testProxySendMsg(String msg) {
//        SendMsgService sendMsgService = (SendMsgService) JdkProxyFactory.getProxy(new SendMsgServiceImpl());
        SendMsgService sendMsgService = (SendMsgService) CglibProxyFactory.getProxy(SendMsgServiceImpl.class);
        return sendMsgService.send(msg);
    }

    public static List<String> getTemporaryDataBatch(List<String> adIdList) {
        List<String> res = Lists.newArrayList();
        adIdList.forEach(adId -> {
            if (adId.length() > 4) {
                wrapperTemporaryDataBatch(adId + "后缀", res);
                return;
            }
            wrapperTemporaryDataBatch(adId, res);
        });
        return res;
    }

    private static void wrapperTemporaryDataBatch(String adId, List<String> adIdList) {
        adIdList.add(adId);
    }

    public static void main(String[] args) {
        ServiceLoader<Superman> serviceLoader = ServiceLoader.load(Superman.class);
        System.out.println("Java SPI:");
        serviceLoader.forEach(Superman::introduce);
    }

    @PostConstruct
    public void init() {
        EXPRESSION_MAP.put("1", "test1");
        EXPRESSION_MAP.put("2", "test2");
        log.warn("=========== load aviator express done ,size:{} ===========", EXPRESSION_MAP.size());
//        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }
}
