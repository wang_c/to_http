package com.wangc.notdemo.service.initialRunner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @author wangchuang
 * @date 2022/3/24 20:28
 * @description: TODO
 */
@Component
@Slf4j
public class ToHttpListener implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {

    }
}
