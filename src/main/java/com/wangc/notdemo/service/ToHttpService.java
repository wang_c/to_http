package com.wangc.notdemo.service;

import com.wangc.notdemo.entity.AdxAgentType;
import com.wangc.notdemo.entity.Company;
import com.wangc.notdemo.entity.params.AgentQueryParam;

/**
 * @author wangchuang
 * @date 2022/3/24 19:52
 * @description: TODO
 */
public interface ToHttpService {
    String toHttp2(String param2, AgentQueryParam param1);
    AdxAgentType toHttp1(AgentQueryParam agentQueryParam, Integer param1, int param2, Double param3);

}
