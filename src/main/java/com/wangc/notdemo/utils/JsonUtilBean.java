package com.wangc.notdemo.utils;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import java.io.Writer;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import com.wangc.notdemo.exception.JsonException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * @author wangchuang
 * @date 2022/4/11 15:09
 * @description: JsonUtilBean
 */
public class JsonUtilBean {
    private Logger logger = LogManager.getLogger(JsonUtilBean.class);
    private ObjectMapper objectMapper = new ObjectMapper();
    boolean isSilent;
    boolean isWarning;

    public JsonUtilBean() {
        this.objectMapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, false);
        this.objectMapper.configure(Feature.ALLOW_SINGLE_QUOTES, true);
        this.objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        this.objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        this.objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        this.objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        this.objectMapper.setFilters((new SimpleFilterProvider()).setFailOnUnknownId(false));
        this.objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
//        this.objectMapper.setDateFormat(new DateFormat());
    }

    public <T> JavaType getListType(Class<T> clz) {
        return this.objectMapper.getTypeFactory().constructParametricType(List.class, new Class[]{clz});
    }

    public <T> JavaType getMapType(Class<T> clz) {
        return this.objectMapper.getTypeFactory().constructParametricType(HashMap.class, new Class[]{String.class, clz});
    }

    public <T> String toJson(T src) {
        if (src == null) {
            return null;
        } else {
            try {
                return src instanceof String ? (String)src : this.objectMapper.writeValueAsString(src);
            } catch (Exception var3) {
                if (this.isSilent) {
                    if (this.isWarning) {
                        this.logger.warn(var3.getMessage(), var3);
                    }

                    return null;
                } else {
                    throw new JsonException(var3);
                }
            }
        }
    }

    public <T> byte[] object2Byte(T src) {
        if (src == null) {
            return null;
        } else {
            try {
                return src instanceof byte[] ? (byte[])((byte[])src) : this.objectMapper.writeValueAsBytes(src);
            } catch (Exception var3) {
                if (this.isSilent) {
                    if (this.isWarning) {
                        this.logger.warn(var3.getMessage(), var3);
                    }

                    return null;
                } else {
                    throw new JsonException(var3);
                }
            }
        }
    }

    public <T> T string2Object(String str, Class<T> clazz) {
        if (str != null && clazz != null) {
            try {
                if (Enum.class.isAssignableFrom(clazz)) {
//                    return Enum.valueOf(clazz, str);
                    return null;
                } else {
                    if (Date.class.isAssignableFrom(clazz) && !str.startsWith("\"") && !str.endsWith("\"")) {
                        str = "\"" + str + "\"";
                        this.logger.warn("trans {} to Data.class", str);
                    }

                    return clazz.equals(String.class) ? (T) str : this.objectMapper.readValue(str, clazz);
                }
            } catch (Exception var4) {
                if (this.isSilent) {
                    if (this.isWarning) {
                        this.logger.warn(JsonException.createMsg(str, clazz, var4), var4);
                    }

                    return null;
                } else {
                    throw new JsonException(str, clazz, var4);
                }
            }
        } else {
            return null;
        }
    }

    public <T> T string2Object(String str, TypeReference<T> typeReference) {
        if (str != null && typeReference != null) {
            try {
                return typeReference.getType().equals(String.class) ? (T) str : this.objectMapper.readValue(str, typeReference);
            } catch (Exception var4) {
                if (this.isSilent) {
                    if (this.isWarning) {
                        this.logger.warn(JsonException.createMsg(str, typeReference, var4), var4);
                    }

                    return null;
                } else {
                    throw new JsonException(str, typeReference, var4);
                }
            }
        } else {
            return null;
        }
    }

    public <T> T string2Object(String str, JavaType typeReference) {
        if (str != null && typeReference != null) {
            try {
                return typeReference.getContentType().equals(String.class) ? (T) str : this.objectMapper.readValue(str, typeReference);
            } catch (Exception var4) {
                if (this.isSilent) {
                    if (this.isWarning) {
                        this.logger.warn(JsonException.createMsg(str, typeReference, var4), var4);
                    }

                    return null;
                } else {
                    throw new JsonException(str, typeReference, var4);
                }
            }
        } else {
            return null;
        }
    }

    public <T> T byte2Object(byte[] bytes, Class<T> clazz) {
        if (bytes != null && clazz != null) {
            try {
                return clazz.equals(byte[].class) ? (T) bytes : this.objectMapper.readValue(bytes, clazz);
            } catch (Exception var4) {
                if (this.isSilent) {
                    if (this.isWarning) {
                        this.logger.warn(JsonException.createMsg(bytes, clazz, var4), var4);
                    }

                    return null;
                } else {
                    throw new JsonException(bytes, clazz, var4);
                }
            }
        } else {
            return null;
        }
    }

    public <T> T byte2Object(byte[] bytes, TypeReference<T> typeReference) {
        if (bytes != null && typeReference != null) {
            try {
                return typeReference.getType().equals(byte[].class) ? (T) bytes : this.objectMapper.readValue(bytes, typeReference);
            } catch (Exception var4) {
                if (this.isSilent) {
                    if (this.isWarning) {
                        this.logger.warn(JsonException.createMsg(bytes, typeReference, var4), var4);
                    }

                    return null;
                } else {
                    throw new JsonException(bytes, typeReference, var4);
                }
            }
        } else {
            return null;
        }
    }

    /** @deprecated */
    @Deprecated
    public <T> T decodeJson(String jsonString, TypeReference<T> tr) {
        if (jsonString != null && !"".equals(jsonString)) {
            try {
                return this.objectMapper.readValue(jsonString, tr);
            } catch (Exception var4) {
                if (this.isSilent) {
                    if (this.isWarning) {
                        this.logger.warn(JsonException.createMsg(jsonString, tr, var4), var4);
                    }

                    return null;
                } else {
                    throw new JsonException(jsonString, tr, var4);
                }
            }
        } else {
            return null;
        }
    }

    public <T> T convertValue(Object fromValue, Class<T> toValueType) {
        try {
            return this.objectMapper.convertValue(fromValue, toValueType);
        } catch (Exception var4) {
            if (this.isSilent) {
                if (this.isWarning) {
                    this.logger.warn(JsonException.createMsg(fromValue, toValueType, var4), var4);
                }

                return null;
            } else {
                throw new JsonException(fromValue, toValueType, var4);
            }
        }
    }

    public <T> T convertValue(Object fromValue, TypeReference toValueTypeRef) {
        try {
            return (T) this.objectMapper.convertValue(fromValue, toValueTypeRef);
        } catch (Exception var4) {
            if (this.isSilent) {
                if (this.isWarning) {
                    this.logger.warn(JsonException.createMsg(fromValue, toValueTypeRef, var4), var4);
                }

                return null;
            } else {
                throw new JsonException(fromValue, toValueTypeRef, var4);
            }
        }
    }

    public void writeValue(Writer w, Object value) {
        try {
            this.objectMapper.writeValue(w, value);
        } catch (Exception var4) {
            if (!this.isSilent) {
                throw new JsonException(value, value.getClass(), var4);
            }

            if (this.isWarning) {
                this.logger.warn(JsonException.createMsg(value, value.getClass(), var4), var4);
            }
        }

    }
}
