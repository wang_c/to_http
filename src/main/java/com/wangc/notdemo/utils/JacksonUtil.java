package com.wangc.notdemo.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author wangchuang
 * @date 2022/2/15 15:23
 * @description: jackson工具类
 */
public class JacksonUtil {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    static  {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNRESOLVED_OBJECT_IDS,false);
        objectMapper.configure(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS,true);
    }

    private JacksonUtil() {}

    public static <T> T beanCopy(Object source, Class<T> target) {
        return objectMapper.convertValue(source, target);
    }

    public static <T> T beanCopy(Object source, TypeReference<T> target) {
        return objectMapper.convertValue(source, target);
    }
}
