package com.wangc.notdemo.utils;

import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.CtField;
import javassist.NotFoundException;
import javassist.CannotCompileException;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.ConstPool;
import javassist.bytecode.MethodInfo;
import javassist.bytecode.ParameterAnnotationsAttribute;
import javassist.bytecode.LocalVariableAttribute;
import javassist.bytecode.AttributeInfo;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.annotation.Annotation;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.agent.ByteBuddyAgent;

/**
 * @author wangchuang
 * @date 2022/4/11 20:23
 * @description: JavassistUtil
 */
@Slf4j
public class JavassistUtil {

    private JavassistUtil() {
    }

    /**
     * 类中的指定方法加指定注解
     * @param clazz 类
     * @param methods 方法
     * @param annotationClass 指定注解
     */
    public static void addAnnotationToMethodParams(Class<?> clazz, Method[] methods, Class<?> annotationClass) {
        try {
            String className =clazz.getName();
            ClassPool pool = ClassPool.getDefault();
            CtClass ctClass = pool.getCtClass(className);
            // 若冻结则先解冻
            if (ctClass.isFrozen()) {
                ctClass.defrost();
            }
            for (Method method : methods) {
                addAnnotationToMethodParam(ctClass, method, annotationClass);
            }
            // 重写class文件
            reTransformClass(Class.forName(className), ctClass.toBytecode());
            ctClass.writeFile("D:\\Desktop");
            // 释放空间，避免ClassPool太大内存溢出
            ctClass.detach();
        } catch (Exception e) {
            log.error("JavassistUtil add annotation to method param error ! {}", e.getMessage());
        }
    }
    /**
     * 为方法的参数加注解
     */
    private static void addAnnotationToMethodParam(CtClass ctClass, Method method, Class<?> annotationClass) {
        try {
            String methodName = method.getName();
            Parameter[] parameters = method.getParameters();
            // 没有参数，直接return
            if (parameters == null || parameters.length == 0) {
                return;
            }
            ConstPool constPool = ctClass.getClassFile().getConstPool();
            CtMethod ctMethod = ctClass.getDeclaredMethod(methodName);
            // 要添加的目标注解
            Annotation toAddAnnotation = new Annotation(annotationClass.getName(), constPool);

            // get方法参数注解  -> 参数add目标注解 -> set到参数注解属性中  -> add参数注解属性到方法中
            ParameterAnnotationsAttribute originParamAnnotationsAttribute = (ParameterAnnotationsAttribute)
                    ctMethod.getMethodInfo().getAttribute(ParameterAnnotationsAttribute.visibleTag);
            Annotation[][] paramAnnotations;
            if (originParamAnnotationsAttribute == null ||
                    originParamAnnotationsAttribute.getAnnotations() == null ||
                    originParamAnnotationsAttribute.getAnnotations().length == 0) {
                paramAnnotations = new Annotation[parameters.length][0];
            } else {
                paramAnnotations = originParamAnnotationsAttribute.getAnnotations();
            }
            for (int i = 0; i < paramAnnotations.length; i++) {
                // 基本类型和String类型不加注解
                if (isBaseType(parameters[i].getType())) {
                    continue;
                }
                int size = paramAnnotations[i].length;
                Annotation[] annotations = new Annotation[size+1];
                System.arraycopy(paramAnnotations[i], 0, annotations, 0, size);
                annotations[size] = toAddAnnotation;
                paramAnnotations[i] = annotations;
            }

            ParameterAnnotationsAttribute newParamAnnotationsAttribute =
                    new ParameterAnnotationsAttribute(constPool, ParameterAnnotationsAttribute.visibleTag);
            newParamAnnotationsAttribute.setAnnotations(paramAnnotations);
            ctMethod.getMethodInfo().addAttribute(newParamAnnotationsAttribute);
        } catch (Exception e){
            log.error("JavassistUtil add annotation to method param error ! {}", e.getMessage());
        }
    }

    public static boolean isBaseType(Class<?> clazz) {
        return clazz.isPrimitive() ||
                clazz.equals(Integer.class) ||
                clazz.equals(Byte.class) ||
                clazz.equals(Long.class) ||
                clazz.equals(Double.class) ||
                clazz.equals(Float.class) ||
                clazz.equals(Character.class) ||
                clazz.equals(Short.class) ||
                clazz.equals(Boolean.class) ||
                clazz.equals(String.class);
    }

    private static void reTransformClass(Class<?> clazz, byte[] byteCode) {
        // 用于改变运行时的字节码，在jvm加载类之前
        ClassFileTransformer classFileTransformer = (loader, className, classBeingRedefined, protectionDomain, classfileBuffer) -> byteCode;

        Instrumentation instrumentation = ByteBuddyAgent.install();
        try {
            // classFileTransformer需要添加到instrumentation中才能生效
            instrumentation.addTransformer(classFileTransformer, true);
            instrumentation.retransformClasses(clazz);
        } catch (UnmodifiableClassException e) {
            e.printStackTrace();
        } finally {
            instrumentation.removeTransformer(classFileTransformer);
        }
    }

    /**
     * 获取方法的参数变量名称
     */
    public static String[] getMethodVariableName(String className,String methodName){
        try {

            ClassPool pool = ClassPool.getDefault();
            CtClass ctClass = pool.get(className);
            if (ctClass.isFrozen()) {
                ctClass.defrost();
            }
            CtMethod ctMethod = ctClass.getDeclaredMethod(methodName);
            // 参数类型
            CtClass[] parameterTypes = ctMethod.getParameterTypes();
            MethodInfo methodInfo = ctMethod.getMethodInfo();
            CodeAttribute codeAttribute = methodInfo.getCodeAttribute();
            String[] paramNames = new String[ctMethod.getParameterTypes().length];

            LocalVariableAttribute attr = (LocalVariableAttribute) codeAttribute.getAttribute(LocalVariableAttribute.tag);
            if (attr != null)  {

                int pos = Modifier.isStatic(ctMethod.getModifiers()) ? 0 : 1;
                for (int i = 0; i < paramNames.length; i++){
                    paramNames[i] = attr.variableName(i + pos);
                }
                for (int i1 = 0; i1 < paramNames.length; i1++) {
                    System.out.printf("%d:[%s %s] ", i1+1, parameterTypes[i1].getName(), paramNames[i1]);
                }
                return paramNames;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    public static void addAnnotationToField(Class<?> clazz, String fieldName, Class<?> annotationClass,
                                            BiConsumer<Annotation, ConstPool> initAnnotation) {
        ClassPool pool = ClassPool.getDefault();
        CtClass ctClass;
        try {
            ctClass = pool.getCtClass(clazz.getName());
            if (ctClass.isFrozen()) {
                ctClass.defrost();
            }
            CtField ctField = ctClass.getDeclaredField(fieldName);
            ConstPool constPool = ctClass.getClassFile().getConstPool();

            Annotation annotation = new Annotation(annotationClass.getName(), constPool);
            if (initAnnotation != null) {
                initAnnotation.accept(annotation, constPool);
            }

            AnnotationsAttribute attr = getAnnotationsAttributeFromField(ctField);
            if (attr == null) {
                attr = new AnnotationsAttribute(constPool, AnnotationsAttribute.visibleTag);
                ctField.getFieldInfo().addAttribute(attr);
            }
            attr.addAnnotation(annotation);

            reTransformClass(clazz, ctClass.toBytecode());
        } catch (NotFoundException | IOException | CannotCompileException e) {
            e.printStackTrace();
        }
    }

    public static void removeAnnotationFromField(Class<?> clazz, String fieldName, Class<?> annotationClass) {
        ClassPool pool = ClassPool.getDefault();
        CtClass ctClass;
        try {
            ctClass = pool.getCtClass(clazz.getName());
            if (ctClass.isFrozen()) {
                ctClass.defrost();
            }
            CtField ctField = ctClass.getDeclaredField(fieldName);

            AnnotationsAttribute attr = getAnnotationsAttributeFromField(ctField);
            if (attr != null) {
                attr.removeAnnotation(annotationClass.getName());
            }

            reTransformClass(clazz, ctClass.toBytecode());
        } catch (NotFoundException | IOException | CannotCompileException e) {
            e.printStackTrace();
        }
    }

    private static AnnotationsAttribute getAnnotationsAttributeFromField(CtField ctField) {
        List<AttributeInfo> attrs = ctField.getFieldInfo().getAttributes();
        AnnotationsAttribute attr = null;
        if (attrs != null) {
            Optional<AttributeInfo> optional = attrs.stream()
                    .filter(AnnotationsAttribute.class::isInstance)
                    .findFirst();
            if (optional.isPresent()) {
                attr = (AnnotationsAttribute) optional.get();
            }
        }
        return attr;
    }
}