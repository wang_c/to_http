package com.wangc.notdemo.utils;

/**
 * @author wangchuang
 * @date 2022/4/11 15:08
 * @description: TODO
 */
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import java.io.Writer;

public class JsonUtil {
    public static final JsonUtilBean silent = new JsonUtilBean();
    public static final JsonUtilBean normal;
    public static final JsonUtilBean warn;

    private JsonUtil() {
    }

    public static <T> String toJson(T src) {
        return normal.toJson(src);
    }

    public static <T> byte[] object2Byte(T src) {
        return normal.object2Byte(src);
    }

    public static <T> T string2Object(String str, Class<T> clazz) {
        return normal.string2Object(str, clazz);
    }

    public static <T> T string2Object(String str, TypeReference<T> typeReference) {
        return normal.string2Object(str, typeReference);
    }

    public static <T> T string2Object(String ext, JavaType mapType) {
        return normal.string2Object(ext, mapType);
    }

    public static <T> T byte2Object(byte[] bytes, Class<T> clazz) {
        return normal.byte2Object(bytes, clazz);
    }

    public static <T> T byte2Object(byte[] bytes, TypeReference<T> typeReference) {
        return normal.byte2Object(bytes, typeReference);
    }

    /** @deprecated */
    @Deprecated
    public static <T> T decodeJson(String jsonString, TypeReference<T> tr) {
        return normal.decodeJson(jsonString, tr);
    }

    public static <T> T convertValue(Object fromValue, Class<T> toValueType) {
        return normal.convertValue(fromValue, toValueType);
    }

    public static <T> T convertValue(Object fromValue, TypeReference toValueTypeRef) {
        return normal.convertValue(fromValue, toValueTypeRef);
    }

    public static <T> JavaType getListType(Class<T> clz) {
        return normal.getListType(clz);
    }

    public static <T> JavaType getMapType(Class<T> clz) {
        return normal.getMapType(clz);
    }

    public static void writeValue(Writer w, Object value) {
        normal.writeValue(w, value);
    }

    static {
        silent.isSilent = true;
        silent.isWarning = false;
        normal = new JsonUtilBean();
        normal.isSilent = false;
        normal.isWarning = false;
        warn = new JsonUtilBean();
        warn.isSilent = true;
        warn.isWarning = true;
    }
}
