package com.wangc.notdemo.exception;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;

/**
 * @author wangchuang
 * @date 2022/4/11 15:10
 * @description: JsonException
 */
public class JsonException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public JsonException() {
    }

    public JsonException(String message) {
        super(message);
    }

    public JsonException(String message, Throwable cause) {
        super(message, cause);
    }

    public JsonException(Throwable cause) {
        super(cause);
    }

    protected JsonException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public JsonException(Object value, Class type, Throwable cause) {
        this(createMsg(value, type, cause), cause);
    }

    public JsonException(Object value, TypeReference type, Throwable cause) {
        this(createMsg(value, type, cause), cause);
    }

    public JsonException(Object value, JavaType type, Throwable cause) {
        this(createMsg(value, type, cause), cause);
    }

    public static String createMsg(Object value, Class type, Throwable cause) {
        return "value [" + value + "] to type [" + type.getName() + "] | " + cause.getMessage();
    }

    public static String createMsg(Object value, TypeReference type, Throwable cause) {
        return "value [" + value + "] to type [" + type.getType() + "] | " + cause.getMessage();
    }

    public static String createMsg(Object value, JavaType type, Throwable cause) {
        return "value [" + value + "] to type [" + type.getTypeName() + "] | " + cause.getMessage();
    }
}