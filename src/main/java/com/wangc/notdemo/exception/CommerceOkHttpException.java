package com.wangc.notdemo.exception;

/**
 * @author wangchuang
 * @date 2022/4/11 15:17
 * @description: TODO
 */
public class CommerceOkHttpException extends RuntimeException {
    public CommerceOkHttpException(String msg) {
        super(msg);
    }

    public CommerceOkHttpException(Throwable e) {
        super(e);
    }
}
