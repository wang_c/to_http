package com.wangc.notdemo.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.wangc.notdemo.entity.AdxAgentType;
import com.wangc.notdemo.entity.TestPo;
import com.wangc.notdemo.entity.params.AgentQueryParam;
import com.wangc.notdemo.http.CommerceOkHttpClientJackson;
import com.wangc.notdemo.http.HttpClient;
import com.wangc.notdemo.service.TestService;
import com.wangc.notdemo.service.ToHttpServiceImpl;
import com.wangc.notdemo.utils.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.annotation.Resource;

@RestController
@Slf4j
@RequestMapping("/test")
public class TestContoller {
    @Resource
    TestService testService;
    @Resource
    HttpClient httpClient;
    @Autowired
    RequestMappingHandlerMapping requestMappingHandlerMapping;

    @PostMapping("/save")
    public String test1(@RequestBody TestPo testPo, @RequestParam String abc) {
        System.out.println(testPo);
        System.out.println(abc);
        return "success";
    }

    @GetMapping("/annotation")
    public String test2() {
        testService.testEncryption1();
        return "success";
    }

    @GetMapping("/proxy/sendMsg")
    public String test3(@RequestParam String msg) {
        return testService.testProxySendMsg(msg);
    }

    @Resource
    CommerceOkHttpClientJackson okHttpClientJackson;

    private static final String URL = "http://localhost:8081/getAgentByParams?param1=1&param2=1&param3=1.2";

    @GetMapping("/test/get")
    public AdxAgentType getMapping(@RequestBody AgentQueryParam agentQueryParam) {
        AdxAgentType result = httpClient.doGetJson(URL, agentQueryParam, new TypeReference<AdxAgentType>() {});
        AdxAgentType result1 = httpClient.doGetJson(URL, agentQueryParam, AdxAgentType.class);
        System.out.println(result);
        System.out.println(result1);
        return result;
    }




    @GetMapping("/dubbo2http")
    public void toHttp(@RequestParam String path) {
        RequestMappingInfo requestMappingInfo = RequestMappingInfo
                .paths(path)
                .methods(RequestMethod.GET)
                .build();
        try {
            // 获取目标处理类的
//            Class<?> myController = TestContoller.class.getClassLoader().loadClass("com.wangc.notdemo.service.ToHttpServiceImpl");
//            Object obj = myController.newInstance();

            //最关键的一步，注册mapping对象
            requestMappingHandlerMapping.registerMapping(requestMappingInfo, ToHttpServiceImpl.class.newInstance(), ToHttpServiceImpl.class.getDeclaredMethod("toHttp2", String.class));
        } catch (Exception e) {
            log.error(e.getMessage());
        }


    }
}
