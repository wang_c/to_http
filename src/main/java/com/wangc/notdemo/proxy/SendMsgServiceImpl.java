package com.wangc.notdemo.proxy;

import lombok.Builder;
import org.springframework.stereotype.Service;

/**
 * @author wangchuang
 * @date 2022/2/16 18:39
 * @description: TODO
 */
@Service
public class SendMsgServiceImpl implements SendMsgService {

    @Override
    public String send(String msg) {
        System.out.println("send msg : " + msg);
        return msg;
    }
}
