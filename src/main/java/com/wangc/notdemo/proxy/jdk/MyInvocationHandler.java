package com.wangc.notdemo.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author wangchuang
 * @date 2022/2/16 18:43
 * @description: TODO
 */
public class MyInvocationHandler implements InvocationHandler {
    private final Object target;

    public MyInvocationHandler(Object target) {
        this.target = target;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws InvocationTargetException, IllegalAccessException {
        System.out.println("jdk proxy, before send msg!");
        Object result = method.invoke(target, args);
        System.out.println("jdk proxy, after send msg!");
        return result;
    }

}
