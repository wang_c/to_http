package com.wangc.notdemo.proxy.jdk;

import java.lang.reflect.Proxy;

/**
 * @author wangchuang
 * @date 2022/2/17 10:59
 * @description: Jdk代理工厂
 */
public class JdkProxyFactory {
    public static Object getProxy(Object target) {
        return Proxy.newProxyInstance(
                target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),
                new MyInvocationHandler(target));
    }
}
