package com.wangc.notdemo.proxy;

/**
 * @author wangchuang
 * @date 2022/2/16 18:39
 * @description: 发送消息接口
 */
public interface SendMsgService {
    String send(String msg);
}
