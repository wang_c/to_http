package com.wangc.notdemo.proxy.cglib;

import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author wangchuang
 * @date 2022/2/17 12:01
 * @description: cglib动态代理-自实现MethodInterceptor
 */
public class MyMethodInterceptor implements MethodInterceptor {
    /**
     * 自定义增强方法
     * @param o 被代理对象
     * @param method 被代理对象的方法
     * @param args 方法入参
     * @param methodProxy 用于调用目标对象的方法
     */
    @Override
    public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        System.out.println("cglib proxy, before method: " + method.getName());
        Object object = methodProxy.invokeSuper(o, args);
        System.out.println("cglib proxy, after method: " + method.getName());
        return object;
    }
}
