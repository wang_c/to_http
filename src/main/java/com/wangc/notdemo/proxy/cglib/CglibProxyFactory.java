package com.wangc.notdemo.proxy.cglib;

import org.springframework.cglib.proxy.Enhancer;

/**
 * @author wangchuang
 * @date 2022/2/17 12:09
 * @description: cglib代理工厂
 */
public class CglibProxyFactory {
    public static Object getProxy(Class<?> clazz) {
        Enhancer enhancer = new Enhancer();
        // 被代理类的类加载器
        enhancer.setClassLoader(clazz.getClassLoader());
        // 被代理类
        enhancer.setSuperclass(clazz);
        //方法拦截器
        enhancer.setCallback(new MyMethodInterceptor());
        return enhancer.create();
    }
}
