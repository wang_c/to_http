package com.wangc.notdemo.http;

/**
 * @author wangchuang
 * @date 2022/4/14 11:00
 * @description: TODO
 */
public class HttpClientException extends RuntimeException {

    public HttpClientException(String msg) {
        super(msg);
    }

    public HttpClientException(Throwable e) {
        super(e);
    }
}
