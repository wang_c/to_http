package com.wangc.notdemo.http;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mysql.cj.x.protobuf.MysqlxDatatypes;
import com.wangc.notdemo.utils.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;

/**
 * @author wangchuang
 * @date 2022/4/12 14:54
 * @description: HttpClient
 */
@Slf4j
@Service
public class HttpClient {

    private static final CloseableHttpClient HTTP_CLIENT = HttpClients.createDefault();

    private static final RequestConfig DEFAULT_REQUEST_CONFIG =  RequestConfig.custom()
            .setSocketTimeout(10 * 1000)
            .setConnectTimeout(10 * 1000)
            .build();

    public String doGetJson(String url, String jsonBody, RequestConfig requestConfig) {
        CloseableHttpResponse response = null;
        try {
            response = HTTP_CLIENT.execute(this.buildHttpGetWithBody(url, jsonBody, requestConfig));
            return getResultFromResponse(response);
        } catch (Exception e) {
            throw new HttpClientException("http client do get with body execute error: " + e.getMessage());
        } finally {
            try {
                if (Objects.nonNull(response)) {
                    response.close();
                }
            } catch (IOException e) {
                log.error("httpclient response close error! {}", e.getMessage());
            }
        }
    }

    public String doGetJson(String url, String jsonBody) {
       return this.doGetJson(url, jsonBody, DEFAULT_REQUEST_CONFIG);
    }

    public <T> T doGetJson(String url, Object param, TypeReference<T> reference) {
        return JsonUtil.string2Object(doGetJson(url, JsonUtil.toJson(param)), reference);
    }

    public <T> T doGetJson(String url, Object param, RequestConfig requestConfig, TypeReference<T> reference) {
        return JsonUtil.string2Object(doGetJson(url, JsonUtil.toJson(param), requestConfig), reference);
    }

    public <T> T doGetJson(String url, Object param, Class<T> tClass) {
        return JsonUtil.string2Object(doGetJson(url, JsonUtil.toJson(param)), tClass);
    }

    public <T> T doGetJson(String url, Object param, RequestConfig requestConfig, Class<T> tClass) {
        return JsonUtil.string2Object(doGetJson(url, JsonUtil.toJson(param),requestConfig), tClass);
    }

    private HttpGetWithBody buildHttpGetWithBody(String url, String jsonBody, RequestConfig requestConfig) {
        HttpGetWithBody httpGetWithEntity = new HttpGetWithBody(url);
        HttpEntity httpEntity = new StringEntity(jsonBody, ContentType.APPLICATION_JSON);
        httpGetWithEntity.setEntity(httpEntity);
        if (Objects.nonNull(requestConfig)) {
            httpGetWithEntity.setConfig(requestConfig);
        }
        return httpGetWithEntity;
    }

    private String getResultFromResponse(CloseableHttpResponse response) {
        try {
            HttpEntity entity = response.getEntity();
            if (Objects.nonNull(entity)) {
                return EntityUtils.toString(entity, StandardCharsets.UTF_8);
            } else {
                throw new HttpClientException("httpClient response entity is null");
            }
        } catch (IOException e) {
            log.error("get result from response error! {}", e.getMessage());
            throw new HttpClientException(e);
        }
    }
}
