package com.wangc.notdemo.http;

import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import java.net.URI;

/**
 * @author wangchuang
 * @date 2022/4/12 14:51
 * @description: HttpGetWithBody
 */
public class HttpGetWithBody extends HttpEntityEnclosingRequestBase {
    private static final String METHOD_NAME = "GET";

    @Override
    public String getMethod() {
        return METHOD_NAME;
    }

    public HttpGetWithBody() {
        super();
    }
    public HttpGetWithBody(final URI uri) {
        super();
        setURI(uri);
    }
    HttpGetWithBody(final String uri) {
        super();
        setURI(URI.create(uri));
    }

}

