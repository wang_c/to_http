package com.wangc.notdemo.http;

import org.springframework.stereotype.Service;

/**
 * @author wangchuang
 * @date 2022/4/11 15:02
 * @description: OkHttpProperties
 */
@Service
public class OkHttpProperties {
    private Integer connectTimeout = 10000;
    private Integer readTimeout = 10000;
    private Integer writeTimeout = 10000;
    private Integer maxIdleConnections = 5;
    private Integer keepAliveDuration = 5;
    private String projectName = "project";
    private boolean retryOnConnectionFailure = true;

    public OkHttpProperties() {
    }

    public Integer getConnectTimeout() {
        return this.connectTimeout;
    }

    public Integer getReadTimeout() {
        return this.readTimeout;
    }

    public Integer getWriteTimeout() {
        return this.writeTimeout;
    }

    public Integer getMaxIdleConnections() {
        return this.maxIdleConnections;
    }

    public Integer getKeepAliveDuration() {
        return this.keepAliveDuration;
    }

    public String getProjectName() {
        return this.projectName;
    }

    public boolean isRetryOnConnectionFailure() {
        return this.retryOnConnectionFailure;
    }

    public void setConnectTimeout(Integer connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public void setReadTimeout(Integer readTimeout) {
        this.readTimeout = readTimeout;
    }

    public void setWriteTimeout(Integer writeTimeout) {
        this.writeTimeout = writeTimeout;
    }

    public void setMaxIdleConnections(Integer maxIdleConnections) {
        this.maxIdleConnections = maxIdleConnections;
    }

    public void setKeepAliveDuration(Integer keepAliveDuration) {
        this.keepAliveDuration = keepAliveDuration;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void setRetryOnConnectionFailure(boolean retryOnConnectionFailure) {
        this.retryOnConnectionFailure = retryOnConnectionFailure;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof OkHttpProperties)) {
            return false;
        } else {
            OkHttpProperties other = (OkHttpProperties)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label87: {
                    Object this$connectTimeout = this.getConnectTimeout();
                    Object other$connectTimeout = other.getConnectTimeout();
                    if (this$connectTimeout == null) {
                        if (other$connectTimeout == null) {
                            break label87;
                        }
                    } else if (this$connectTimeout.equals(other$connectTimeout)) {
                        break label87;
                    }

                    return false;
                }

                Object this$readTimeout = this.getReadTimeout();
                Object other$readTimeout = other.getReadTimeout();
                if (this$readTimeout == null) {
                    if (other$readTimeout != null) {
                        return false;
                    }
                } else if (!this$readTimeout.equals(other$readTimeout)) {
                    return false;
                }

                label73: {
                    Object this$writeTimeout = this.getWriteTimeout();
                    Object other$writeTimeout = other.getWriteTimeout();
                    if (this$writeTimeout == null) {
                        if (other$writeTimeout == null) {
                            break label73;
                        }
                    } else if (this$writeTimeout.equals(other$writeTimeout)) {
                        break label73;
                    }

                    return false;
                }

                Object this$maxIdleConnections = this.getMaxIdleConnections();
                Object other$maxIdleConnections = other.getMaxIdleConnections();
                if (this$maxIdleConnections == null) {
                    if (other$maxIdleConnections != null) {
                        return false;
                    }
                } else if (!this$maxIdleConnections.equals(other$maxIdleConnections)) {
                    return false;
                }

                label59: {
                    Object this$keepAliveDuration = this.getKeepAliveDuration();
                    Object other$keepAliveDuration = other.getKeepAliveDuration();
                    if (this$keepAliveDuration == null) {
                        if (other$keepAliveDuration == null) {
                            break label59;
                        }
                    } else if (this$keepAliveDuration.equals(other$keepAliveDuration)) {
                        break label59;
                    }

                    return false;
                }

                Object this$projectName = this.getProjectName();
                Object other$projectName = other.getProjectName();
                if (this$projectName == null) {
                    if (other$projectName != null) {
                        return false;
                    }
                } else if (!this$projectName.equals(other$projectName)) {
                    return false;
                }

                if (this.isRetryOnConnectionFailure() != other.isRetryOnConnectionFailure()) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof OkHttpProperties;
    }

    public int hashCode() {
        int result = 1;
        Object $connectTimeout = this.getConnectTimeout();
        result = result * 59 + ($connectTimeout == null ? 43 : $connectTimeout.hashCode());
        Object $readTimeout = this.getReadTimeout();
        result = result * 59 + ($readTimeout == null ? 43 : $readTimeout.hashCode());
        Object $writeTimeout = this.getWriteTimeout();
        result = result * 59 + ($writeTimeout == null ? 43 : $writeTimeout.hashCode());
        Object $maxIdleConnections = this.getMaxIdleConnections();
        result = result * 59 + ($maxIdleConnections == null ? 43 : $maxIdleConnections.hashCode());
        Object $keepAliveDuration = this.getKeepAliveDuration();
        result = result * 59 + ($keepAliveDuration == null ? 43 : $keepAliveDuration.hashCode());
        Object $projectName = this.getProjectName();
        result = result * 59 + ($projectName == null ? 43 : $projectName.hashCode());
        result = result * 59 + (this.isRetryOnConnectionFailure() ? 79 : 97);
        return result;
    }

    public String toString() {
        return "OkHttpProperties(connectTimeout=" + this.getConnectTimeout() + ", readTimeout=" + this.getReadTimeout() + ", writeTimeout=" + this.getWriteTimeout() + ", maxIdleConnections=" + this.getMaxIdleConnections() + ", keepAliveDuration=" + this.getKeepAliveDuration() + ", projectName=" + this.getProjectName() + ", retryOnConnectionFailure=" + this.isRetryOnConnectionFailure() + ")";
    }
}
