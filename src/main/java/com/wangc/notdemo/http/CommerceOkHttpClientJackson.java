package com.wangc.notdemo.http;

/**
 * @author wangchuang
 * @date 2022/4/11 14:58
 * @description: CommerceOkHttpClientJackson
 */
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import com.wangc.notdemo.exception.CommerceOkHttpException;
import com.wangc.notdemo.utils.JsonUtil;
import okhttp3.*;
import okhttp3.OkHttpClient.Builder;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class CommerceOkHttpClientJackson {
    private static final Logger log = LoggerFactory.getLogger(CommerceOkHttpClientJackson.class);
    @Resource
    OkHttpProperties okHttpProperties;
    private static OkHttpClient okHttpClient;

    public CommerceOkHttpClientJackson() {
    }

    @PostConstruct
    private void init() {
        okHttpClient = (new Builder()).connectTimeout((long)this.okHttpProperties.getConnectTimeout(), TimeUnit.MILLISECONDS).readTimeout((long)this.okHttpProperties.getReadTimeout(), TimeUnit.MILLISECONDS).writeTimeout((long)this.okHttpProperties.getWriteTimeout(), TimeUnit.MILLISECONDS).connectionPool(new ConnectionPool(this.okHttpProperties.getMaxIdleConnections(), (long)this.okHttpProperties.getKeepAliveDuration(), TimeUnit.MINUTES)).retryOnConnectionFailure(this.okHttpProperties.isRetryOnConnectionFailure()).build();
    }

    public String doGet(String url, Map<String, String> params, Map<String, String> headers, OkHttpClient okHttpClient) {
        try {
            Response response = okHttpClient.newCall(this.buildGetRequest(url, params, headers)).execute();
            if (Objects.nonNull(response) && this.isOkResponseCode(response.code())) {
                return response.body().string();
            } else {
                throw new RuntimeException("reponse=" + response + ", body=" + this.getBodyString(response));
            }
        } catch (Exception var6) {
            throw new CommerceOkHttpException(var6);
        }
    }

    public String doGetObjectParam(String url, Object params, Map<String, String> headers, OkHttpClient okHttpClient) {
        try {
            Response response = okHttpClient.newCall(this.buildGetJsonRequest(url, params, headers)).execute();
            if (Objects.nonNull(response) && this.isOkResponseCode(response.code())) {
                return response.body().string();
            } else {
                throw new RuntimeException("reponse=" + response + ", body=" + this.getBodyString(response));
            }
        } catch (Exception var6) {
            throw new CommerceOkHttpException(var6);
        }
    }

    public String doGet(String url, Map<String, String> params, Map<String, String> headers) {
        return this.doGet(url, params, headers, okHttpClient);
    }

    public String doGetObjectParam(String url, Object params, Map<String, String> headers) {
        return this.doGetObjectParam(url, params, headers, okHttpClient);
    }

    private String getBodyString(Response response) throws IOException {
        return Objects.nonNull(response) && Objects.nonNull(response.body()) ? response.body().string() : null;
    }

    public <T> T doGet(String url, Map<String, String> params, Map<String, String> headers, Class<T> tClass) {
        return JsonUtil.string2Object(this.doGet(url, params, headers), tClass);
    }

    public <T> T doGet(String url, Map<String, String> params, Map<String, String> headers, Class<T> tClass, OkHttpClient okHttpClient) {
        return JsonUtil.string2Object(this.doGet(url, params, headers, okHttpClient), tClass);
    }

    public <T> T doGet(String url, Map<String, String> params, Map<String, String> headers, TypeReference<T> reference) {
        return JsonUtil.string2Object(this.doGet(url, params, headers), reference);
    }

    public <T> T doGetObjectParam(String url, Object params, Map<String, String> headers, TypeReference<T> reference) {
        return JsonUtil.string2Object(this.doGetObjectParam(url, params, headers), reference);
    }

    public <T> T doGet(String url, Map<String, String> params, Map<String, String> headers, TypeReference<T> reference, OkHttpClient okHttpClient) {
        return JsonUtil.string2Object(this.doGet(url, params, headers, okHttpClient), reference);
    }

    public String doDelete(String url, Map<String, String> params, Map<String, String> headers, OkHttpClient okHttpClient) {
        try {
            Response response = okHttpClient.newCall(this.buildDeleteRequest(url, params, headers)).execute();
            if (Objects.nonNull(response) && this.isOkResponseCode(response.code())) {
                return response.body().string();
            } else {
                throw new RuntimeException("reponse=" + response + ", body=" + this.getBodyString(response));
            }
        } catch (Exception var6) {
            throw new CommerceOkHttpException(var6);
        }
    }

    public String doDelete(String url, Map<String, String> params, Map<String, String> headers) {
        return this.doDelete(url, params, headers, okHttpClient);
    }

    public <T> T doDelete(String url, Map<String, String> params, Map<String, String> headers, Class<T> tClass) {
        return JsonUtil.string2Object(this.doDelete(url, params, headers), tClass);
    }

    public <T> T doDelete(String url, Map<String, String> params, Map<String, String> headers, Class<T> tClass, OkHttpClient okHttpClient) {
        return JsonUtil.string2Object(this.doDelete(url, params, headers, okHttpClient), tClass);
    }

    public <T> T doDelete(String url, Map<String, String> params, Map<String, String> headers, TypeReference<T> reference) {
        return JsonUtil.string2Object(this.doDelete(url, params, headers), reference);
    }

    public <T> T doDelete(String url, Map<String, String> params, Map<String, String> headers, TypeReference<T> reference, OkHttpClient okHttpClient) {
        return JsonUtil.string2Object(this.doDelete(url, params, headers, okHttpClient), reference);
    }

    public String doPostForm(String url, Object params, Map<String, String> headers, OkHttpClient okHttpClient) {
        try {
            Response response = okHttpClient.newCall(this.buildPostFormRequest(url, params, headers)).execute();
            if (Objects.nonNull(response) && response.code() == HttpStatus.OK.value()) {
                return response.body().string();
            } else {
                throw new RuntimeException("reponse=" + response + ", body=" + this.getBodyString(response));
            }
        } catch (Exception var6) {
            throw new CommerceOkHttpException(var6);
        }
    }

    public String doPostForm(String url, Object params, Map<String, String> headers) {
        return this.doPostForm(url, params, headers, okHttpClient);
    }

    public <T> T doPostForm(String url, Object params, Map<String, String> headers, Class<T> tClass) {
        return JsonUtil.string2Object(this.doPostForm(url, params, headers), tClass);
    }

    public <T> T doPostForm(String url, Object params, Map<String, String> headers, Class<T> tClass, OkHttpClient okHttpClient) {
        return JsonUtil.string2Object(this.doPostForm(url, params, headers, okHttpClient), tClass);
    }

    public <T> T doPostForm(String url, Object params, Map<String, String> headers, TypeReference<T> reference) {
        return JsonUtil.string2Object(this.doPostForm(url, params, headers), reference);
    }

    public <T> T doPostForm(String url, Object params, Map<String, String> headers, TypeReference<T> reference, OkHttpClient okHttpClient) {
        return JsonUtil.string2Object(this.doPostForm(url, params, headers, okHttpClient), reference);
    }

    public String doPutJson(String url, Object params, Map<String, String> headers, OkHttpClient okHttpClient) {
        try {
            Response response = okHttpClient.newCall(this.buildPutJsonRequest(url, params, headers)).execute();
            if (Objects.nonNull(response) && this.isOkResponseCode(response.code())) {
                return response.body().string();
            } else {
                throw new RuntimeException("reponse=" + response + ", body=" + this.getBodyString(response));
            }
        } catch (Exception var6) {
            throw new CommerceOkHttpException(var6);
        }
    }

    public String doPutJson(String url, Object params, Map<String, String> headers) {
        return this.doPutJson(url, params, headers, okHttpClient);
    }

    public String doPutForm(String url, Object params, Map<String, String> headers, OkHttpClient okHttpClient) {
        try {
            Response response = okHttpClient.newCall(this.buildPutFormRequest(url, params, headers)).execute();
            if (Objects.nonNull(response) && response.code() == HttpStatus.OK.value()) {
                return response.body().string();
            } else {
                throw new RuntimeException("reponse=" + response + ", body=" + this.getBodyString(response));
            }
        } catch (Exception var6) {
            throw new CommerceOkHttpException(var6);
        }
    }

    public String doPutForm(String url, Object params, Map<String, String> headers) {
        return this.doPutForm(url, params, headers, okHttpClient);
    }

    public <T> T doPutForm(String url, Object params, Map<String, String> headers, Class<T> tClass) {
        return JsonUtil.string2Object(this.doPutForm(url, params, headers), tClass);
    }

    public <T> T doPutForm(String url, Object params, Map<String, String> headers, Class<T> tClass, OkHttpClient okHttpClient) {
        return JsonUtil.string2Object(this.doPutForm(url, params, headers, okHttpClient), tClass);
    }

    public <T> T doPutForm(String url, Object params, Map<String, String> headers, TypeReference<T> reference) {
        return JsonUtil.string2Object(this.doPutForm(url, params, headers), reference);
    }

    public <T> T doPutForm(String url, Object params, Map<String, String> headers, TypeReference<T> reference, OkHttpClient okHttpClient) {
        return JsonUtil.string2Object(this.doPutForm(url, params, headers, okHttpClient), reference);
    }

    private Request buildPostFormRequest(String url, Object params, Map<String, String> headers) {
        Map<String, Object> jsonObject = (Map)JsonUtil.convertValue(params, new TypeReference<Map<String, Object>>() {
        });
        okhttp3.FormBody.Builder formbodyBuilder = new okhttp3.FormBody.Builder();
        Iterator var6 = jsonObject.keySet().iterator();

        while(var6.hasNext()) {
            String key = (String)var6.next();
            formbodyBuilder.add(key, MapUtils.getString(jsonObject, key));
        }

        okhttp3.Request.Builder builder = (new okhttp3.Request.Builder()).post(formbodyBuilder.build()).addHeader("Content-Type", "application/x-www-form-urlencoded").url(url);
        if (MapUtils.isNotEmpty(headers)) {
            headers.forEach(builder::addHeader);
        }

        this.addUAHeader(builder);
        return builder.build();
    }

    public static void main(String[] arvg) throws NoSuchFieldException, IllegalAccessException {
        HashMap<Object, Object> map = Maps.newHashMap();
        map.put("1", 2);
        map.put("jackon", "3");
        OkHttpProperties okHttpProperties = new OkHttpProperties();
        okHttpProperties.setConnectTimeout(1);
        okHttpProperties.setProjectName("123");
        CommerceOkHttpClientJackson client = new CommerceOkHttpClientJackson();
        client.okHttpProperties = okHttpProperties;
        Request request = client.buildPostFormRequest("http://www.baidu.com", map, (Map)null);
        System.out.println(request);
        Field n = FormBody.class.getDeclaredField("encodedNames");
        n.setAccessible(true);
        Field v = FormBody.class.getDeclaredField("encodedValues");
        v.setAccessible(true);
        System.out.println(n.get(request.body()));
        System.out.println(v.get(request.body()));
        request = client.buildPostFormRequest("http://www.baidu.com", okHttpProperties, (Map)null);
        System.out.println(request);
        System.out.println(n.get(request.body()));
        System.out.println(v.get(request.body()));
    }

    public String doPostJson(String url, Object params, Map<String, String> headers, OkHttpClient okHttpClient) {
        try {
            Response response = okHttpClient.newCall(this.buildPostJsonRequest(url, params, headers)).execute();
            if (Objects.nonNull(response) && this.isOkResponseCode(response.code())) {
                return response.body().string();
            } else {
                throw new RuntimeException("reponse=" + response + ", body=" + this.getBodyString(response));
            }
        } catch (Exception var6) {
            throw new CommerceOkHttpException(var6);
        }
    }

    private boolean isOkResponseCode(Integer responseCode) {
        return responseCode >= 200 && responseCode < 300;
    }

    public String doPostJson(String url, Object params, Map<String, String> headers) {
        return this.doPostJson(url, params, headers, okHttpClient);
    }

    public <T> T doPostJson(String url, Object params, Map<String, String> headers, Class<T> tClass) {
        return JsonUtil.string2Object(this.doPostJson(url, params, headers), tClass);
    }

    public <T> T doPostJson(String url, Object params, Map<String, String> headers, Class<T> tClass, OkHttpClient okHttpClient) {
        return JsonUtil.string2Object(this.doPostJson(url, params, headers, okHttpClient), tClass);
    }

    public <T> T doPostJson(String url, Object params, Map<String, String> headers, TypeReference<T> reference) {
        return JsonUtil.string2Object(this.doPostJson(url, params, headers), reference);
    }

    public <T> T doPostJson(String url, Object params, Map<String, String> headers, TypeReference<T> reference, OkHttpClient okHttpClient) {
        return JsonUtil.string2Object(this.doPostJson(url, params, headers, okHttpClient), reference);
    }

    private Request buildPostJsonRequest(String url, Object param, Map<String, String> headers) {
        okhttp3.Request.Builder builder = (new okhttp3.Request.Builder()).post(FormBody.create(MediaType.parse("application/json"), JsonUtil.toJson(param))).addHeader("Content-Type", "application/json").url(url);
        if (MapUtils.isNotEmpty(headers)) {
            headers.forEach(builder::addHeader);
        }

        this.addUAHeader(builder);
        return builder.build();
    }

    private Request buildPutJsonRequest(String url, Object param, Map<String, String> headers) {
        okhttp3.Request.Builder builder = (new okhttp3.Request.Builder()).put(FormBody.create(MediaType.parse("application/json"), JsonUtil.toJson(param))).addHeader("Content-Type", "application/json").url(url);
        if (MapUtils.isNotEmpty(headers)) {
            headers.forEach(builder::addHeader);
        }

        this.addUAHeader(builder);
        return builder.build();
    }

    public <T> T doPutJson(String url, Object params, Map<String, String> headers, Class<T> tClass) {
        return JsonUtil.string2Object(this.doPutJson(url, params, headers), tClass);
    }

    public <T> T doPutJson(String url, Object params, Map<String, String> headers, Class<T> tClass, OkHttpClient okHttpClient) {
        return JsonUtil.string2Object(this.doPutJson(url, params, headers, okHttpClient), tClass);
    }

    public <T> T doPutJson(String url, Object params, Map<String, String> headers, TypeReference<T> reference) {
        return JsonUtil.string2Object(this.doPutJson(url, params, headers), reference);
    }

    public <T> T doPutJson(String url, Object params, Map<String, String> headers, TypeReference<T> reference, OkHttpClient okHttpClient) {
        return JsonUtil.string2Object(this.doPutJson(url, params, headers, okHttpClient), reference);
    }

    private Request buildPutFormRequest(String url, Object params, Map<String, String> headers) {
        Map<String, Object> jsonObject = (Map)JsonUtil.convertValue(params, new TypeReference<Map<String, Object>>() {
        });
        okhttp3.FormBody.Builder formbodyBuilder = new okhttp3.FormBody.Builder();
        Iterator var6 = jsonObject.keySet().iterator();

        while(var6.hasNext()) {
            String key = (String)var6.next();
            formbodyBuilder.add(key, MapUtils.getString(jsonObject, key));
        }

        okhttp3.Request.Builder builder = (new okhttp3.Request.Builder()).put(formbodyBuilder.build()).addHeader("Content-Type", "application/x-www-form-urlencoded").url(url);
        if (MapUtils.isNotEmpty(headers)) {
            headers.forEach(builder::addHeader);
        }

        this.addUAHeader(builder);
        return builder.build();
    }

    private Request buildDeleteRequest(String url, Map<String, String> params, Map<String, String> headers) {
        if (MapUtils.isNotEmpty(params)) {
            List<String> paramLists = Lists.newArrayList();
            params.forEach((key, value) -> {
                paramLists.add(key + "=" + value);
            });
            url = url + "?" + Joiner.on("&").join(paramLists);
        }

        okhttp3.Request.Builder builder = (new okhttp3.Request.Builder()).url(url);
        if (StringUtils.isEmpty(MapUtils.getString(headers, "Content-Type"))) {
            builder.addHeader("Content-Type", "application/x-www-form-urlencoded");
        }

        if (MapUtils.isNotEmpty(headers)) {
            headers.forEach(builder::addHeader);
        }

        this.addUAHeader(builder);
        return builder.delete().build();
    }

    private Request buildGetRequest(String url, Map<String, String> params, Map<String, String> headers) {
        if (MapUtils.isNotEmpty(params)) {
            List<String> paramLists = Lists.newArrayList();
            params.forEach((key, value) -> {
                paramLists.add(key + "=" + value);
            });
            url = url + "?" + Joiner.on("&").join(paramLists);
        }

        okhttp3.Request.Builder builder = (new okhttp3.Request.Builder()).url(url);
        if (StringUtils.isEmpty(MapUtils.getString(headers, "Content-Type"))) {
            builder.addHeader("Content-Type", "application/x-www-form-urlencoded");
        }

        if (MapUtils.isNotEmpty(headers)) {
            headers.forEach(builder::addHeader);
        }

        this.addUAHeader(builder);
        return builder.get().build();
    }

    private Request buildGetJsonRequest(String url, Object param, Map<String, String> headers) {
        okhttp3.Request.Builder builder = (new okhttp3.Request.Builder()).url(url);
        RequestBody requestBody = FormBody.create(MediaType.parse("application/json"), JsonUtil.toJson(param));
        builder.method("GET", requestBody);
        builder.addHeader("Content-Type", "application/json");
        if (MapUtils.isNotEmpty(headers)) {
            headers.forEach(builder::addHeader);
        }

        this.addUAHeader(builder);
        return builder.build();
    }

    private void addUAHeader(okhttp3.Request.Builder builder) {
        builder.addHeader("User-Agent", "/commerce/ad/" + this.okHttpProperties.getProjectName());
    }

    public static OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }

    private Request encodeAndbuildGetRequest(String url, Map<String, String> params, Map<String, String> headers) {
        HttpUrl httpUrl = HttpUrl.parse(url);
        if (MapUtils.isNotEmpty(params)) {
            params.forEach((key, value) -> {
                httpUrl.newBuilder().addQueryParameter(key, value);
            });
        }

        okhttp3.Request.Builder builder = (new okhttp3.Request.Builder()).addHeader("Content-Type", "application/x-www-form-urlencoded").url(httpUrl);
        if (MapUtils.isNotEmpty(headers)) {
            headers.forEach(builder::addHeader);
        }

        this.addUAHeader(builder);
        return builder.get().build();
    }

    public String encodeAndDoGet(String url, Map<String, String> params, Map<String, String> headers) {
        try {
            Response response = okHttpClient.newCall(this.encodeAndbuildGetRequest(url, params, headers)).execute();
            if (Objects.nonNull(response) && response.code() == HttpStatus.OK.value()) {
                return response.body().string();
            } else {
                throw new RuntimeException("reponse=" + response + ", body=" + this.getBodyString(response));
            }
        } catch (Exception var5) {
            throw new CommerceOkHttpException(var5);
        }
    }
}